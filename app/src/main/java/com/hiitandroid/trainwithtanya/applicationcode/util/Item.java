package com.hiitandroid.trainwithtanya.applicationcode.util;

import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by alikhan on 11/8/15.
 */
public interface Item {
    public int getViewType();
    public View getView(LayoutInflater inflater, View convertView);
}
