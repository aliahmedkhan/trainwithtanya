/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hiitandroid.trainwithtanya.applicationcode.ui;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.os.AsyncTask;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

import com.hiitandroid.trainwithtanya.applicationcode.BuildConfig;
import com.hiitandroid.trainwithtanya.applicationcode.R;
import com.hiitandroid.trainwithtanya.applicationcode.provider.Images;
import com.hiitandroid.trainwithtanya.applicationcode.provider.Workout;
import com.hiitandroid.trainwithtanya.applicationcode.util.ImageCache;
import com.hiitandroid.trainwithtanya.applicationcode.util.ImageFetcher;
import com.hiitandroid.trainwithtanya.applicationcode.util.Utils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class WorkoutDetailsActivity extends FragmentActivity implements OnClickListener, WorkoutDetailsFragment.OnStartWorkoutListener,
        ExerciseModule.OnModuleFinishedListener, ExercisePreview.OnPreviewFinishedListener {
    private static final String IMAGE_CACHE_DIR = "images";
    public static final String EXTRA_IMAGE = "extra_image";
    public static final String WORKOUT_NUMBER = "workout_number";
    private static final String TAG = "WorkoutDetailsActivity";

    private ImagePagerAdapter mAdapter;
    private ImageFetcher mImageFetcher;
    private ViewPager mPager;
    private BaseApplication mApplication;
    private Workout mWorkout;
    private int mRest;
    private int mExerciseDuration;
    private int mRounds;
    private int mCurrentRound;
    private int mExerciseScreens;
    private int mCurrentExercise;
    private List<Fragment> exerciseFragments;
    private final Object exercisesProcessingLock = new Object();


    @TargetApi(VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG) {
            Utils.enableStrictMode();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_detail_pager);
        mApplication = (BaseApplication) getApplication();

        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);
        mImageFetcher.setImageFadeIn(false);

        // Set up ViewPager and backing adapter
        mAdapter = new ImagePagerAdapter(getSupportFragmentManager(), mApplication.imageSize());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        mPager.setPageMargin((int) getResources().getDimension(R.dimen.horizontal_page_margin));
        mPager.setOffscreenPageLimit(2);

        // Set up activity to go full screen
        getWindow().addFlags(LayoutParams.FLAG_FULLSCREEN);

        // Enable some additional newer visibility and ActionBar features to create a more
        // immersive photo viewing experience
        if (Utils.hasHoneycomb()) {
            final ActionBar actionBar = getActionBar();

            // Hide title text and set home as up
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);

            // Hide and show the ActionBar as the visibility changes
            mPager.setOnSystemUiVisibilityChangeListener(
                    new View.OnSystemUiVisibilityChangeListener() {
                        @Override
                        public void onSystemUiVisibilityChange(int vis) {
                            if ((vis & View.SYSTEM_UI_FLAG_LOW_PROFILE) != 0) {
                                actionBar.hide();
                            } else {
                                actionBar.show();
                            }
                        }
                    });

            // Start low profile mode and hide ActionBar
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
            actionBar.hide();
        }

        // Set the current item based on the extra passed in to this activity
        final int extraCurrentItem = getIntent().getIntExtra(EXTRA_IMAGE, -1);
        if (extraCurrentItem != -1) {
            mPager.setCurrentItem(extraCurrentItem);
        }
        synchronized (mApplication.retrivingInitialWorkoutsLock) {
            Log.v(TAG, "workoutdetailsactivity size = " + mApplication.workoutSize());
            mWorkout = mApplication.getWorkout(getIntent().getIntExtra(WORKOUT_NUMBER, 0));
            mRest = mWorkout.getExerciseRest();
            mExerciseDuration = mWorkout.getExerciseDuration();
            mRounds = mWorkout.getRecommendedRounds();
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Exercise");
            query.whereContainedIn("objectId", mWorkout.getExerciseList());
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(final List<ParseObject> list, ParseException e) {
                    new SetupExercisesSequence().execute(list);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
        if (exerciseFragments != null) {
            exerciseFragments.clear();
        }
        exerciseFragments = null;
        mApplication = null;

        Log.v(TAG, "this is called");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.clear_cache:
                mImageFetcher.clearCache();
                Toast.makeText(
                        this, R.string.clear_cache_complete_toast,Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * Called by the ViewPager child fragments to load images via the one ImageFetcher
     */
    public ImageFetcher getImageFetcher() {
        return mImageFetcher;
    }


    /**
     * The main adapter that backs the ViewPager. A subclass of FragmentStatePagerAdapter as there
     * could be a large number of items in the ViewPager and we don't want to retain them all in
     * memory at once but create/destroy them on the fly.
     */
    private class ImagePagerAdapter extends FragmentStatePagerAdapter {
        private final int mSize;

        public ImagePagerAdapter(FragmentManager fm, int size) {
            super(fm);
            mSize = size;
        }

        @Override
        public int getCount() {
            return mSize;
        }

        @Override
        public Fragment getItem(int position) {
            synchronized (mApplication.retrivingInitialWorkoutsLock) {
                return WorkoutDetailsFragment.newInstance(mApplication.getWorkoutImageUrls(position), mWorkout.getWorkoutDescription(), mWorkout.getWorkoutTitle(), mWorkout.getRecommendedRounds());
            }
        }
    }

    /**
     * Set on the ImageView in the ViewPager children fragments, to enable/disable low profile mode
     * when the ImageView is touched.
     */
    @TargetApi(VERSION_CODES.HONEYCOMB)
    @Override
    public void onClick(View v) {
        final int vis = mPager.getSystemUiVisibility();
        if ((vis & View.SYSTEM_UI_FLAG_LOW_PROFILE) != 0) {
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        } else {
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        }
    }


    private class SetupExercisesSequence extends AsyncTask<List<ParseObject>, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(List<ParseObject>... params) {
            synchronized (exercisesProcessingLock) {
                exerciseFragments = new ArrayList<Fragment>();
                mCurrentRound = 0;
                int exerciseNumber = 0;
                for (ParseObject exercise : params[0]) {
                    ExercisePreview epFragment = ExercisePreview.newInstance(exercise.getString("exerciseTitle"), exercise.getParseFile("exerciseVideoAndroid").getUrl(), mRest);
                    ExerciseModule emFragment = ExerciseModule.newInstance(exercise.getString("exerciseTitle"), exercise.getParseFile("exerciseVideoAndroid").getUrl(),
                            mCurrentRound, mRounds, mExerciseDuration, exerciseNumber);
                    exerciseFragments.add(epFragment);
                    exerciseFragments.add(emFragment);
                    exerciseNumber++;
                }
                mExerciseScreens = exerciseFragments.size();
                Log.v(TAG, "Done");
            }

            return null;
        }


    }

    private void transitionNextExerciseScreen() {
        mCurrentExercise++;
        if (mCurrentExercise < mExerciseScreens && mCurrentRound < mRounds) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment nextExerciseScreen = exerciseFragments.get(mCurrentExercise);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.parentExerciseFragment, nextExerciseScreen);
            fragmentTransaction.commit();
        }
        else if (mCurrentExercise == mExerciseScreens && mCurrentRound < mRounds) {
            mCurrentRound++;
            mCurrentExercise = 0;
            transitionNextExerciseScreen();
        }

        else if (mCurrentRound == mRounds) {
            Log.v(TAG, "workout complete");
            saveCompletedProgress();
        }

        else {
            Log.e(TAG, "transition exercise conditions are not inclusive and/or one or more attributes are broken");
        }
    }

    private void saveCompletedProgress() {
        ParseUser user = ParseUser.getCurrentUser();
        if (user != null) {
            int previouslyCompletedRounds = ((int)user.get("roundsCount"));
            user.put("roundsCount", previouslyCompletedRounds + mCurrentRound);
            if (mCurrentRound == mRounds) {
                int previouslyCompletedWorkouts = (int)user.get("workoutsCount");
                user.put("workoutsCount", ++previouslyCompletedWorkouts);
            }
            user.saveInBackground();
            Log.v(TAG, "saved completed progress");
            updateCompletedWorkouts();
        }
        else {
            Log.i(TAG, "No information to save b/c user not logged in");
        }
    }

    private void updateCompletedWorkouts() {
        final ParseObject completedWorkout = new ParseObject("CompletedWorkout");
        completedWorkout.put("duration", (mCurrentRound * 6 * 30) + (mCurrentExercise * 30));
        completedWorkout.put("rounds", mCurrentRound);
        completedWorkout.put("user", ParseUser.getCurrentUser());
        completedWorkout.put("workout", mWorkout.getParseWorkout());
        completedWorkout.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.v(TAG, "saved successfully workout");
            }
        });
    }
    @Override
    public void onStartWorkout() {
        //setContentView(R.layout.exercises_processing_fragment);
        synchronized (exercisesProcessingLock) {
            mCurrentExercise = -1;
            setContentView(R.layout.fragment_exercise_container);
            transitionNextExerciseScreen();
            Log.i(TAG, "Ali workout started");
        }
    }

    @Override
    public void onModuleFinished() {
        transitionNextExerciseScreen();
    }

    @Override
    public void onWorkoutExited() {
        saveCompletedProgress();
        super.onBackPressed();
    }

    @Override
    public void onPreviewFinished() {
        transitionNextExerciseScreen();
    }

    @Override
    public void onWorkoutExitedFromPreview() {
        super.onBackPressed();
    }


}
