/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hiitandroid.trainwithtanya.applicationcode.ui;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.hiitandroid.trainwithtanya.applicationcode.BuildConfig;
import com.hiitandroid.trainwithtanya.applicationcode.R;
import com.hiitandroid.trainwithtanya.applicationcode.provider.Workout;
import com.hiitandroid.trainwithtanya.applicationcode.util.IabException;
import com.hiitandroid.trainwithtanya.applicationcode.util.IabHelper;
import com.hiitandroid.trainwithtanya.applicationcode.util.IabResult;
import com.hiitandroid.trainwithtanya.applicationcode.util.Inventory;
import com.hiitandroid.trainwithtanya.applicationcode.util.Utils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Simple FragmentActivity to hold the main {@link WorkoutsFragment} and not much else.
 */
public class WorkoutsActivity extends FragmentActivity implements PremiumNotificationFragment.OnPremiumNotifDismissedListener {
    private static final String TAG = "WorkoutsActivity";
    private BaseApplication mApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG) {
            Utils.enableStrictMode();
        }
        super.onCreate(savedInstanceState);
        mApplication = (BaseApplication)getApplication();
        restartIfRunningLowOnMemory();
        Log.v(TAG, "onCreate workoutsActivity");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Workout");
        query.whereEqualTo("public", true);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> list, ParseException e) {
                new SetupInitialWorkoutSceenTask().execute(list);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        restartIfRunningLowOnMemory();
    }

    public void restartIfRunningLowOnMemory() {
        if((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) >= (.85 * Runtime.getRuntime().maxMemory())) {
            Intent mStartActivity = new Intent(getApplicationContext(), WorkoutsActivity.class);
            int mPendingIntentId = 123456;
            PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    @Override
    public void onPremiumNotifDismissed(Uri uri) {

    }

    private class SetupInitialWorkoutSceenTask extends AsyncTask<List<ParseObject>, Void, Void> {

        @Override
        protected Void doInBackground(List<ParseObject>... parseBackEnd) {
            Log.v(TAG, "async task executes");
            if (!mApplication.isParseWorkoutsDownloaded()) {
                synchronized (mApplication.retrivingInitialWorkoutsLock) {
                    Log.v(TAG, "sync block");
                    retrievePurchasedItems();
                    for (ParseObject wkout : parseBackEnd[0]) {
                        //Log.v(TAG, "this ali happens");
                        //Log.v(TAG, "workoutsactivity size = " + mApplication.workoutSize());
                        Workout w = new Workout();
                        w.setParseWorkout(wkout);
                        w.setObjectId(wkout.getObjectId());
                        w.setExerciseDuration(wkout.getInt("exerciseDuration"));
                        w.setExerciseList(wkout.getList("exerciseList"));
                        w.setExerciseRest(wkout.getInt("exerciseInt"));
                        w.setPremium(wkout.getBoolean("premium"));
                        w.setRecommendedRounds(wkout.getInt("recommendedRounds"));
                        w.setWorkoutDescription(wkout.getString("workoutDescription"));
                        w.setWorkoutImage(wkout.getParseFile("workoutImage").getUrl());
                        mApplication.addImage(w.getWorkoutImage());
                        w.setWorkoutTitle(wkout.getString("workoutTitle"));
                        w.setWorkoutVideo(wkout.getParseFile("workoutVideo").getUrl());
                        w.setCreatedAt(wkout.getCreatedAt());
                        w.setUpdatedAt(wkout.getUpdatedAt());
                        mApplication.addWorkout(w);
                    }

                }

                mApplication.setParseWorkoutsDownloaded(true);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (getSupportFragmentManager().findFragmentByTag(TAG) == null) {
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(android.R.id.content, new WorkoutsFragment(), TAG);
                ft.commitAllowingStateLoss();
            }
        }

        private void retrievePurchasedItems() {
            StringBuilder base64EncodedPublicKey = new StringBuilder();
            base64EncodedPublicKey.append("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmmMp4xTKKZVwSLFTnxDdjxVviWyx7KPgA8JXRmocvItrbWlvmUGeIwDdaPbc7+0F/");
            base64EncodedPublicKey.append("X25t1VkoezoF53IXB89Vc6Al9nJ9jKyIan6y1T0SkpsooGU6+v9CVUgdOPYtyZS0Wx59nBDUb1aIbuMh/P+dA4Zb8fRAjI+bbFXr3T6OEO4oc4T");
            base64EncodedPublicKey.append("2AIQXAPwg9dz2XSeA8aY0+MUTQcsr2uxPI4b5Bzxcwu/YpSk6y5Lcvdc42VrqbE6F0/EsWP6CBYC+2LbJRYAhdSqiP1EFswBlfvEuyDpwQ+wj/");
            base64EncodedPublicKey.append("bQNpRI0SpnuAqm2D5cvutS012IKlIov8aKxJ2GP21Rgq3Of4Sssbos7wIDAQAB");
            final IabHelper mHelper = new IabHelper(getApplicationContext(), base64EncodedPublicKey.toString());
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                public void onIabSetupFinished(IabResult result) {
                    if (!result.isSuccess()) {
                        // Oh noes, there was a problem.
                        Log.d(TAG, "Problem setting up In-app Billing: " + result);
                    } else {
                        // Hooray, IAB is fully set up!
                        Log.v(TAG, "Ali happens");

                        Inventory userInv;
                        try {
                            userInv = mHelper.queryInventory(true, null, null);
                            mApplication.setIsPremiumMember(userInv.hasPurchase(getString(R.string.TWELVE_MONTH_SKU)) || userInv.hasPurchase(getString(R.string.THREE_MONTH_SKU)));
                            mHelper.dispose();

                        } catch (IabException ex) {

                        }
                    }
                }
            });


        }
    }
}

