package com.hiitandroid.trainwithtanya.applicationcode.ui;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.hiitandroid.trainwithtanya.applicationcode.provider.Images;
import com.hiitandroid.trainwithtanya.applicationcode.provider.Workout;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.uservoice.uservoicesdk.Config;
import com.uservoice.uservoicesdk.UserVoice;

import java.util.ArrayList;

/**
 * Created by alikhan on 11/4/15.
 */
public class BaseApplication extends Application {
    private ArrayList<Workout> list;
    private boolean parseWorkoutsDownloaded;
    private Images images;
    public Object retrivingInitialWorkoutsLock = new Object();
    private Boolean isPremium = false;
    @Override
    public void onCreate() {
        // begin add
        try {
            Class.forName("android.os.AsyncTask");
        } catch(Throwable ignore) {
        }
        // end add

        super.onCreate();
        Parse.initialize(this, "O9YWsxNxANq1Ss4wGb8uMXcFhpDqi1tDFcVgEf0A", "fbc9tKVJi4g2Dgub5nCgFg1Bu0NQHyaAfWgEnC62");
        list = new ArrayList<Workout>();
        images = new Images();
        parseWorkoutsDownloaded = false;
        FacebookSdk.sdkInitialize(getApplicationContext());
        ParseFacebookUtils.initialize(getApplicationContext());
        Config config = new Config("trainwithtanyaapp.uservoice.com");
        config.setShowForum(false);
        config.setShowPostIdea(false);
        UserVoice.init(config, this);
    }

    public void addWorkout(Workout w) {
        list.add(w);
    }

    public void addImage(String url) {
        images.addImage(url);
    }

    public int imageSize() {
        return images.size();
    }

    public String getWorkoutImageUrls(int index) {
        return images.get(index);
    }

    public boolean isParseWorkoutsDownloaded() {
        return parseWorkoutsDownloaded;
    }

    public void setParseWorkoutsDownloaded(boolean parseWorkoutsDownloaded) {
        this.parseWorkoutsDownloaded = parseWorkoutsDownloaded;
    }

    public Workout getWorkout(int index) {
        return list.get(index);
    }

    public int workoutSize() {
        return list.size();
    }

    public Boolean getIsPremiumMember() {
        return isPremium;
    }

    public void setIsPremiumMember(Boolean isPremium) {
        this.isPremium = isPremium;
    }
}
