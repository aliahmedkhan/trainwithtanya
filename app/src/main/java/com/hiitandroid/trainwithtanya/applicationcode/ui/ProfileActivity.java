package com.hiitandroid.trainwithtanya.applicationcode.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.hiitandroid.trainwithtanya.applicationcode.R;
import com.hiitandroid.trainwithtanya.applicationcode.provider.Workout;
import com.hiitandroid.trainwithtanya.applicationcode.util.Header;
import com.hiitandroid.trainwithtanya.applicationcode.util.HeaderListView;
import com.hiitandroid.trainwithtanya.applicationcode.util.Item;
import com.hiitandroid.trainwithtanya.applicationcode.util.ListItem;
import com.hiitandroid.trainwithtanya.applicationcode.util.RoundedImageView;
import com.hiitandroid.trainwithtanya.applicationcode.util.SectionAdapter;
import com.hiitandroid.trainwithtanya.applicationcode.util.TwoTextArrayAdapter;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileActivity extends Activity {
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private static String TAG = "";
    private final List<String> permissions = Arrays.asList("email", "public_profile", "user_friends");
    private TextView mCompletedStuff;
    private Resources mResources;
    private Bitmap mProfilePicture;
    private List<String> sections;
    private Map<Integer, List<String>> items;
    private BaseApplication mApplication;
    private Context mContext;
    private int mCompletedWorkouts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        mResources = getResources();
        sections = new ArrayList<String>();
        mContext = getApplicationContext();
        items = new HashMap<Integer, List<String>>();
        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.v(TAG, "login success");
                createParseUserAndLinkFB(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
                ParseUser.logOut();
                Log.v(TAG, "login failed");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.v(TAG, "login error");
            }
        });



    }

    @Override
    public void onResume() {
        super.onResume();
        populateScreenIfLoggedIn();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds itemss to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void createParseUserAndLinkFB(AccessToken thisAccessToken) {
        ParseFacebookUtils.logInInBackground(thisAccessToken, new LogInCallback() {
            @Override
            public void done(final ParseUser user, ParseException err) {
                Log.d("MyApp", "this call bakc is called");
                if (user == null) {
                    Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
                    ParseUser.logOut();
                } else if (user.isNew()) {
                    Log.d("MyApp", "User signed up and logged in through Facebook!");
                    GraphRequest request = GraphRequest.newMeRequest(
                            AccessToken.getCurrentAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(
                                        JSONObject object,
                                        GraphResponse response) {
                                    String email = "";
                                    String facebookId = "";
                                    String firstName = "";
                                    String lastName = "";
                                    String gender = "";
                                    String profileUrl = "";

                                    try {
                                        email = object.getString("email");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        facebookId = object.getString("id");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        firstName = object.getString("first_name");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        lastName = object.getString("last_name");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        gender = object.getString("gender");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        profileUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    Log.v(TAG, "profileUrl = " + profileUrl);
                                    if (email.length() > 1) {
                                        user.put("email", email);
                                    }
                                    user.put("facebookId", facebookId);
                                    user.put("firstName", firstName);
                                    user.put("lastName", lastName);
                                    user.put("gender", gender);
                                    user.put("roundsCount", 0);
                                    user.put("workoutsCount", 0);
                                    user.put("androidProfilePictureUrl", profileUrl);
                                    user.saveInBackground(new SaveCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                Log.e("MyApp", "SUCCESS");
                                                Log.v(TAG, "parse user saved it really fucking worked");
                                                loginButton.setVisibility(View.INVISIBLE);
                                            } else {
                                                Log.e("MyApp", "FAILED" + e.getMessage() + Integer.toString(e.getCode()));
                                                // Sign up didn't succeed. Look at the ParseException
                                                // to figure out what went wrong
                                            }
                                        }
                                    });

                                    Log.v("MyApp", object.toString());
                                    Log.v("MyApp", response.toString());

                                }
                            });


                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,first_name,last_name,gender,email, picture");
                    request.setParameters(parameters);
                    request.executeAsync();

                    if (!ParseFacebookUtils.isLinked(user)) {
                        ParseFacebookUtils.linkWithReadPermissionsInBackground(user, ProfileActivity.this, permissions, new SaveCallback() {
                            @Override
                            public void done(ParseException ex) {
                                if (ParseFacebookUtils.isLinked(user)) {
                                    Log.d("MyApp", "Woohoo, user logged in with Facebook!");
                                }
                            }
                        });
                    }

                    postUserInfoOnUI(user);
                } else {
                    Log.v("MyApp", "user is logged in");
                    postUserInfoOnUI(user);
                }
            }
        });
    }

    private void populateScreenIfLoggedIn() {
        ParseUser thisUser = ParseUser.getCurrentUser();
        if(thisUser != null) {
            postUserInfoOnUI(thisUser);
        }
        else {
            LoginManager.getInstance().logOut();
            ParseUser.logOut();
            mCompletedStuff = (TextView) findViewById(R.id.profile_completed_text);
            mCompletedStuff.setVisibility(View.INVISIBLE);
        }
    }

    private void postUserInfoOnUI(final ParseUser thisUser) {
        //text of rounds and workouts completed
        final int workouts = thisUser.getInt("workoutsCount");
        final int rounds = thisUser.getInt("roundsCount");
        final String pictureUrl = thisUser.getString("androidProfilePictureUrl");
        if (mProfilePicture == null && pictureUrl != null && pictureUrl.contains("http")) {
            new ImageLoadTask(pictureUrl, ((RoundedImageView) findViewById(R.id.profile_picture))).execute();
        }

        ParseQuery<ParseObject> pastWorkoutsQuery = ParseQuery.getQuery("CompletedWorkout");
        pastWorkoutsQuery.whereEqualTo("user", thisUser);
        pastWorkoutsQuery.orderByAscending("createdAt");
        pastWorkoutsQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (objects.size() != mCompletedWorkouts) {
                    Log.v(TAG, "completed list refreshed");
                    if (items.size() > 0) {
                        items.clear();
                    }
                    if (sections.size() > 0) {
                        sections.clear();
                    }
                    new RetrieveCompletedWorkoutsTask().execute(objects);
                }
            }
        });





        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCompletedStuff = (TextView) findViewById(R.id.profile_completed_text);
                StringBuilder sb = new StringBuilder(mResources.getQuantityString(R.plurals.workouts_and_rounds_completed, workouts, workouts, rounds));
                mCompletedStuff.setText(sb.substring(1, sb.length()));
                loginButton.setVisibility(View.INVISIBLE);
                mCompletedStuff.setVisibility(View.VISIBLE);
            }
        });

    }

    private class ImageLoadTask extends AsyncTask<Void, Void, Void> {

        private String url;
        private RoundedImageView imageView;

        public ImageLoadTask(String url, RoundedImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                mProfilePicture = BitmapFactory.decodeStream(input);
                return null;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (mProfilePicture != null) {
                imageView.setImageBitmap(mProfilePicture);
            }
        }

    }

    private class CompletedWorkoutsRow{
        private String imageUrl;
        private String workoutTitle;
        private String duration;
        private String rounds;
    }

    private class RetrieveCompletedWorkoutsTask extends AsyncTask<List<ParseObject>, Void, Void> {

        @Override
        protected Void doInBackground(List<ParseObject>... params) {
            mCompletedWorkouts = params[0].size();
            Date runningSection = null;
            int day = 0;
            int month = 0;
            int date = 0;
            int sectionNumber = -1;
            List<String> currentSectionItems = new ArrayList<String>();
            for (ParseObject completedTask : params[0]) {
                Date currentDate = completedTask.getCreatedAt();
                if (currentDate.getDay() != day || currentDate.getDate() != date || currentDate.getMonth() != month || runningSection == null ) {
                    if (sectionNumber != -1) {
                        items.put(sectionNumber, currentSectionItems);
                        currentSectionItems = new ArrayList<String>();
                    }
                    runningSection = currentDate;
                    day = runningSection.getDay();
                    month = runningSection.getMonth();
                    date = runningSection.getDate();
                    sections.add(getDateString(day, month, date));
                    sectionNumber++;
                }
                ParseQuery<ParseObject> thisWorkoutQuery = ParseQuery.getQuery("Workout");
                ParseObject p = (ParseObject)completedTask.get("workout");
                thisWorkoutQuery.whereEqualTo("objectId", p.getObjectId());
                ParseObject thisWorkout = null;
                try {
                    thisWorkout = thisWorkoutQuery.getFirst();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
                if (thisWorkout != null) {
                    StringBuilder rowContent = new StringBuilder();
                    rowContent.append(thisWorkout.getString("workoutTitle"));
                    rowContent.append("\n");
                    rowContent.append(completedTask.getInt("duration") / 60);
                    rowContent.append(" minutes | ");
                    rowContent.append(completedTask.getInt("rounds"));
                    rowContent.append(" rounds");
                    currentSectionItems.add(rowContent.toString());
                    //Log.v(TAG, "rowcontent " + rowContent.toString());
                }
                else {
                    Log.e(TAG, "error in retrieving workout for specific date");
                    sections.remove(sections.size()-1);
                }
            }

            if (sectionNumber != -1) {
                items.put(sectionNumber, currentSectionItems);
                currentSectionItems = new ArrayList<String>();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    HeaderListView l = (HeaderListView) findViewById(R.id.listView);
                    l.setAdapter(new SectionAdapter(sections, items) {
                        @Override
                        public int numberOfSections() {
                            return sections.size();
                        }

                        @Override
                        public int numberOfRows(int section) {
                            if (items.get(section) != null) {
                                return items.get(section).size();
                            }
                            return 0;
                        }

                        @Override
                        public Object getRowItem(int section, int row) {
                            return null;
                        }

                        @Override
                        public boolean hasSectionHeaderView(int section) {
                            return true;
                        }

                        @Override
                        public View getRowView(int section, int row, View convertView, ViewGroup parent) {
                            if (convertView == null) {
                                convertView = (TextView) getLayoutInflater().inflate(getResources().getLayout(R.layout.completed_workout_item), null);
                            }
                            if (section < numberOfSections() && row < numberOfRows(section)) {
                                ((TextView) convertView).setText(items.get(section).get(row));
                            }
                            return convertView;
                        }

                        @Override
                        public int getSectionHeaderViewTypeCount() {
                            return 2;
                        }

                        @Override
                        public int getSectionHeaderItemViewType(int section) {
                            return section % 2;
                        }

                        @Override
                        public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {

                            if (convertView == null) {
                                convertView = getLayoutInflater().inflate(getResources().getLayout(R.layout.header), null);
                            }


                            TextView text = (TextView) convertView.findViewById(R.id.separator);
                            if (section < numberOfSections()) {
                                text.setText(sections.get(section));
                            }
                            return convertView;
                        }

                    });
                    l.setVisibility(View.VISIBLE);
                }
            });
        }

        private String getDateString(int day, int month, int date) {
            StringBuilder dateString = new StringBuilder();
            switch(day) {
                case Calendar.MONDAY:
                    dateString.append("Mon");
                    break;
                case Calendar.TUESDAY:
                    dateString.append("Tue");
                    break;
                case Calendar.WEDNESDAY:
                    dateString.append("Wed");
                    break;
                case Calendar.THURSDAY:
                    dateString.append("Thu");
                    break;
                case Calendar.FRIDAY:
                    dateString.append("Fri");
                    break;
                case Calendar.SATURDAY:
                    dateString.append("Sat");
                    break;
                case Calendar.SUNDAY:
                    dateString.append("Sun");
                    break;
            }

            dateString.append(" ");

            switch(month) {
                case Calendar.JANUARY:
                    dateString.append("Jan");
                    break;
                case Calendar.FEBRUARY:
                    dateString.append("Feb");
                    break;
                case Calendar.MARCH:
                    dateString.append("Mar");
                    break;
                case Calendar.APRIL:
                    dateString.append("Apr");
                    break;
                case Calendar.MAY:
                    dateString.append("May");
                    break;
                case Calendar.JUNE:
                    dateString.append("Jun");
                    break;
                case Calendar.JULY:
                    dateString.append("Jul");
                    break;
                case Calendar.AUGUST:
                    dateString.append("Aug");
                    break;
                case Calendar.SEPTEMBER:
                    dateString.append("Sep");
                    break;
                case Calendar.OCTOBER:
                    dateString.append("Oct");
                    break;
                case Calendar.NOVEMBER:
                    dateString.append("Nov");
                    break;
                case Calendar.DECEMBER:
                    dateString.append("Dec");
                    break;
            }

            dateString.append(" ");
            dateString.append(date);
            return dateString.toString();
        }
    }


}


