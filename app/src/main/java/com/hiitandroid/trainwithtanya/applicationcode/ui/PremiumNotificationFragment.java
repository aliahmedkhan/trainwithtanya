package com.hiitandroid.trainwithtanya.applicationcode.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.hiitandroid.trainwithtanya.applicationcode.BuildConfig;
import com.hiitandroid.trainwithtanya.applicationcode.R;
import com.hiitandroid.trainwithtanya.applicationcode.util.IabHelper;
import com.hiitandroid.trainwithtanya.applicationcode.util.IabResult;
import com.hiitandroid.trainwithtanya.applicationcode.util.ImageCache;
import com.hiitandroid.trainwithtanya.applicationcode.util.ImageFetcher;
import com.hiitandroid.trainwithtanya.applicationcode.util.Purchase;
import com.hiitandroid.trainwithtanya.applicationcode.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnPremiumNotifDismissedListener} interface
 * to handle interaction events.
 * Use the {@link PremiumNotificationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PremiumNotificationFragment extends Fragment implements AdapterView.OnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnPremiumNotifDismissedListener mListener;

    private ImageFetcher mImageFetcher;
    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private static final String IMAGE_CACHE_DIR = "thumbs1";
    private ImageAdapter mAdapter;
    private List<String> mUrls;
    private int mWidth;
    private int mHeight;
    private Button mCancelButton;
    private Button mSubOne;
    private Button mSubTwo;
    StringBuilder base64EncodedPublicKey;
    IabHelper mHelper;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PremiumNotificationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PremiumNotificationFragment newInstance(String param1, String param2) {
        PremiumNotificationFragment fragment = new PremiumNotificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public PremiumNotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        WindowManager wm = (WindowManager) getActivity().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        //Point size = new Point();
        //display.getRealSize(size);
        mWidth = display.getWidth();// size.x;
        mHeight = display.getHeight(); /// size.y;
        mUrls = new ArrayList<>();
        mUrls.add("http://i288.photobucket.com/albums/ll185/akhan007/Screen%20Shot%202015-11-08%20at%205.19.56%20PM_zpsjqhggrdu.png");
        mUrls.add("http://i288.photobucket.com/albums/ll185/akhan007/Screen%20Shot%202015-11-08%20at%205.20.29%20PM_zpsujhi5vjb.png");
        mUrls.add("http://i288.photobucket.com/albums/ll185/akhan007/Screen%20Shot%202015-11-08%20at%205.20.56%20PM_zpsf4xnfj7s.png");
        mUrls.add("http://i288.photobucket.com/albums/ll185/akhan007/Screen%20Shot%202015-11-08%20at%205.21.12%20PM_zpsoxmrunyj.png");


        mAdapter = new ImageAdapter(getActivity());
        //mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
        // = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);
        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(getActivity(), IMAGE_CACHE_DIR);

        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(getActivity(), mWidth, mHeight);
        mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(getActivity().getSupportFragmentManager(), cacheParams);

        base64EncodedPublicKey = new StringBuilder();
        base64EncodedPublicKey.append("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmmMp4xTKKZVwSLFTnxDdjxVviWyx7KPgA8JXRmocvItrbWlvmUGeIwDdaPbc7+0F/");
        base64EncodedPublicKey.append("X25t1VkoezoF53IXB89Vc6Al9nJ9jKyIan6y1T0SkpsooGU6+v9CVUgdOPYtyZS0Wx59nBDUb1aIbuMh/P+dA4Zb8fRAjI+bbFXr3T6OEO4oc4T");
        base64EncodedPublicKey.append("2AIQXAPwg9dz2XSeA8aY0+MUTQcsr2uxPI4b5Bzxcwu/YpSk6y5Lcvdc42VrqbE6F0/EsWP6CBYC+2LbJRYAhdSqiP1EFswBlfvEuyDpwQ+wj/");
        base64EncodedPublicKey.append("bQNpRI0SpnuAqm2D5cvutS012IKlIov8aKxJ2GP21Rgq3Of4Sssbos7wIDAQAB");
        // compute your public key and store it in base64EncodedPublicKey
        mHelper = new IabHelper(getContext(), base64EncodedPublicKey.toString());
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d("", "Problem setting up In-app Billing: " + result);
                }
                // Hooray, IAB is fully set up!
                Log.d("", "In-app billing setup");
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_premium_notification, container, false);
        mCancelButton = (Button) v.findViewById(R.id.exit_premium_screen);
        mSubOne = (Button) v.findViewById(R.id.button_sub_one);
        mSubTwo = (Button) v.findViewById(R.id.button_sub_two);
        final GridView mGridView = (GridView) v.findViewById(R.id.gridView);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(this);
        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                // Pause fetcher to ensure smoother scrolling when flinging
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                    // Before Honeycomb pause image loading on scroll to help with performance
                    if (!Utils.hasHoneycomb()) {
                        mImageFetcher.setPauseWork(true);
                    }
                } else {
                    mImageFetcher.setPauseWork(false);
                }


            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
//                Log.v("", "firstVisbileItem + visibleitemCount + totalitemCount " + firstVisibleItem + " " + visibleItemCount + " "  +  totalItemCount);
                if (firstVisibleItem == 4 && visibleItemCount == 1) {
                    mSubOne.setVisibility(View.VISIBLE);
                    mSubTwo.setVisibility(View.VISIBLE);
                }
                else {
                    mSubOne.setVisibility(View.INVISIBLE);
                    mSubTwo.setVisibility(View.INVISIBLE);
                }
            }
        });

        // This listener is used to get the final width of the GridView and then calculate the
        // number of columns and the width of each column. The width of each column is variable
        // as the GridView has stretchMode=columnWidth. The column width is used to set the height
        // of each view so we get nice square thumbnails.
        mGridView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onGlobalLayout() {
                        if (mAdapter.getNumColumns() == 0) {
                            final int numColumns = 1;
                            if (numColumns > 0) {
                                final int columnWidth =
                                        (mGridView.getWidth() / numColumns) - mImageThumbSpacing;
                                mAdapter.setNumColumns(numColumns);
                                mAdapter.setItemHeight(mHeight);
                                if (BuildConfig.DEBUG) {
                                    Log.d("", "onCreateView - numColumns set to " + numColumns);
                                }
                                if (Utils.hasJellyBean()) {
                                    mGridView.getViewTreeObserver()
                                            .removeOnGlobalLayoutListener(this);
                                } else {
                                    mGridView.getViewTreeObserver()
                                            .removeGlobalOnLayoutListener(this);
                                }
                            }
                        }
                    }
                });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        mImageFetcher.setPauseWork(false);
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
        if (mHelper != null) {
            mHelper.dispose();
        }
        mHelper = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

    }
    @Override
    public void onStart() {
        super.onStart();
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        mSubOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("", "Subone");
                //TODO stop user from multiple touches, cancel old process start new one
                mHelper.launchPurchaseFlow(getActivity(), getString(R.string.THREE_MONTH_SKU), 10001,
                        mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
            }
        });
        mSubTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("", "Subtwo");
                mHelper.launchPurchaseFlow(getActivity(), getString(R.string.TWELVE_MONTH_SKU), 10001,
                        mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onPremiumNotifDismissed(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPremiumNotifDismissedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPremiumNotifDismissedListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase)
        {
            if (result.isFailure()) {
                Log.d("", "Error purchasing: " + result);
                return;
            }
            else if (purchase.getSku().equals(getString(R.string.TWELVE_MONTH_SKU))) {
                // give user access to premium content for 3 months and update the UI
                Log.v("", "3 months bought");
            }
            else if (purchase.getSku().equals(getString(R.string.THREE_MONTH_SKU))) {
                // give user access to premium content for 12 months and update the UI
                Log.v("", "12 months bought");
            }
        }
    };

    /**
     * The main adapter that backs the GridView. This is fairly standard except the number of
     * columns in the GridView is used to create a fake top row of empty views as we use a
     * transparent ActionBar and don't want the real top row of images to start off covered by it.
     */
    private class ImageAdapter extends BaseAdapter {

        private final Context mContext;
        private int mItemHeight = 0;
        private int mNumColumns = 0;
        private int mActionBarHeight = 0;
        private GridView.LayoutParams mImageViewLayoutParams;

        public ImageAdapter(Context context) {
            super();
            mContext = context;
            mImageViewLayoutParams = new GridView.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            // Calculate ActionBar height
            TypedValue tv = new TypedValue();
            if (context.getTheme().resolveAttribute(
                    android.R.attr.actionBarSize, tv, true)) {
                mActionBarHeight = TypedValue.complexToDimensionPixelSize(
                        tv.data, context.getResources().getDisplayMetrics());
            }
        }

        @Override
        public int getCount() {
            // If columns have yet to be determined, return no items
            if (getNumColumns() == 0) {
                return 0;
            }

            // Size + number of columns for top empty row
            return mUrls.size() + mNumColumns;
        }

        @Override
        public Object getItem(int position) {
            return position < mNumColumns ?
                    null : mUrls.get(position - mNumColumns);
        }

        @Override
        public long getItemId(int position) {
            return position < mNumColumns ? 0 : position - mNumColumns;
        }

        @Override
        public int getViewTypeCount() {
            // Two types of views, the normal ImageView and the top row of empty views
            return 2;
        }

        @Override
        public int getItemViewType(int position) {
            return (position < mNumColumns) ? 1 : 0;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup container) {
            //BEGIN_INCLUDE(load_gridview_item)
            // First check if this is the top row
            if (position < mNumColumns) {
                if (convertView == null) {
                    convertView = new View(mContext);
                }
                // Set empty view with height of ActionBar
//                convertView.setLayoutParams(new AbsListView.LayoutParams(
//                        ViewGroup.LayoutParams.MATCH_PARENT, mActionBarHeight));
                return convertView;
            }

            // Now handle the main ImageView thumbnails
            ImageView imageView;
            if (convertView == null) { // if it's not recycled, instantiate and initialize
                imageView = new RecyclingImageView(mContext);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setLayoutParams(mImageViewLayoutParams);
            } else { // Otherwise re-use the converted view
                imageView = (ImageView) convertView;
            }

            // Check the height matches our calculated column width
            if (imageView.getLayoutParams().height != mItemHeight) {
                imageView.setLayoutParams(mImageViewLayoutParams);
            }

            // Finally load the image asynchronously into the ImageView, this also takes care of
            // setting a placeholder image while the background thread runs
            mImageFetcher.loadImage(mUrls.get(position - mNumColumns), imageView);
            return imageView;
            //END_INCLUDE(load_gridview_item)
        }

        /**
         * Sets the item height. Useful for when we know the column width so the height can be set
         * to match.
         *
         * @param height
         */
        public void setItemHeight(int height) {
            if (height == mItemHeight) {
                return;
            }
            mItemHeight = height;
            mImageViewLayoutParams =
                    new GridView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mItemHeight);
            mImageFetcher.setImageSize(height);
            notifyDataSetChanged();
        }

        public void setNumColumns(int numColumns) {
            mNumColumns = numColumns;
        }

        public int getNumColumns() {
            return mNumColumns;
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnPremiumNotifDismissedListener {
        // TODO: Update argument type and name
        public void onPremiumNotifDismissed(Uri uri);
    }

}
