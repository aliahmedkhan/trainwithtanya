package com.hiitandroid.trainwithtanya.applicationcode.ui;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.hiitandroid.trainwithtanya.applicationcode.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ExercisePreview.OnPreviewFinishedListener} interface
 * to handle interaction events.
 * Use the {@link ExercisePreview#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExercisePreview extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TITLE = "mWorkoutTitle";
    private static final String ARG_URL = "mVideoUrl";
    private static final String ARG_REST = "mWorkoutRest";
    private static final String TAG = "ExercisePreviewFragment";

    // TODO: Rename and change types of parameters
    private String          mWorkoutTitle;
    private String          mVideoUrl;
    private int             mWorkoutRest;
    private CountDownTimer  mCountDownTimer;
    private VideoView       mVideo;

    private OnPreviewFinishedListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param workoutTitle Parameter 1.
     * @param videoUrl Parameter 2.
     * @return A new instance of fragment ExercisePreview.
     */
    public static ExercisePreview newInstance(String workoutTitle, String videoUrl, int workoutRest) {
        ExercisePreview fragment = new ExercisePreview();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, workoutTitle);
        args.putString(ARG_URL, videoUrl);
        args.putInt(ARG_REST, workoutRest);
        fragment.setArguments(args);
        return fragment;
    }

    public ExercisePreview() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mWorkoutTitle = getArguments().getString(ARG_TITLE);
            mVideoUrl = getArguments().getString(ARG_URL);
            mWorkoutRest = getArguments().getInt(ARG_REST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exercise_preview, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView exerciseTitleLabel = (TextView) getView().findViewById(R.id.preview_workoutTitle);
        TextView end = (TextView) getView().findViewById(R.id.preview_end);
        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onWorkoutExitedFromPreview();
            }
        });

        exerciseTitleLabel.setText(mWorkoutTitle);
        final TextView timer = (TextView) getView().findViewById(R.id.preview_timer);
        startVideo();
        mCountDownTimer = new CountDownTimer(11000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timer.setText(Integer.toString((int) millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                if (mListener != null) {
                    mVideo.suspend();
                    mListener.onPreviewFinished();
                }
            }
        }.start();


    }

    private void startVideo() {
        Uri uri = Uri.parse(mVideoUrl);
        mVideo = (VideoView) getView().findViewById(R.id.preview_videoView);
        mVideo.setVideoURI(uri);
        mVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        mVideo.start();
    }

    private void endVideo() {
        mVideo.suspend();
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onPreviewFinished();
//        }
//    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPreviewFinishedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnPreviewFinishedListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mVideo.destroyDrawingCache();
        Log.v(TAG, "detach preview occurs");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *
     * TODO this could be the way I update the bar on the top
     */
    public interface OnPreviewFinishedListener {
        // TODO: Update argument type and name
        public void onPreviewFinished();
        public void onWorkoutExitedFromPreview();
    }

}
