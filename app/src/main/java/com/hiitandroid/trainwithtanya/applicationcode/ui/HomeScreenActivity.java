package com.hiitandroid.trainwithtanya.applicationcode.ui;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

import com.hiitandroid.trainwithtanya.applicationcode.R;


public class HomeScreenActivity extends TabActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        setupTabs();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
//        AppEventsLogger.activateApp(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupTabs() {
        TabHost tabs = getTabHost(); //(TabHost)findViewById(android.R.id.tabhost);
        tabs.setup();

        // Workouts
        TabHost.TabSpec workoutsTab = tabs.newTabSpec("workouts");
        Intent workoutsIntent = new Intent(this, WorkoutsActivity.class);
        workoutsTab.setContent(workoutsIntent);
        workoutsTab.setIndicator("Workouts");


        // Profile
        TabHost.TabSpec profileTab = tabs.newTabSpec("profile");
        Intent profileIntent = new Intent(this, ProfileActivity.class);
        profileTab.setContent(profileIntent);
        profileTab.setIndicator("Profile");


        // Settings
        TabHost.TabSpec settingsTab = tabs.newTabSpec("settings");
        Intent settingsIntent = new Intent(this, SettingsActivity.class);
        settingsTab.setContent(settingsIntent);
        settingsTab.setIndicator("Settings");

        tabs.addTab(workoutsTab);
        tabs.addTab(profileTab);
        tabs.addTab(settingsTab);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
//        AppEventsLogger.deactivateApp(this);
    }
}
