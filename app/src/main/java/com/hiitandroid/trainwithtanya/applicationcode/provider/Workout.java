package com.hiitandroid.trainwithtanya.applicationcode.provider;

import com.parse.ParseObject;

import java.util.Date;
import java.util.List;

/**
 * Created by alikhan on 11/4/15.
 */
public class Workout {
    private String objectId;
    private int exerciseDuration;
    private List<Object> exerciseList;
    private int exerciseRest;
    private boolean premium;
    private int recommendedRounds;
    private String workoutDescription;
    private String workoutImage;
    private String workoutTitle;
    private String workoutVideo;
    private Date createdAt;
    private Date updatedAt;



    private ParseObject parseWorkout;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public int getExerciseDuration() {
        return exerciseDuration;
    }

    public void setExerciseDuration(int exerciseDuration) {
        this.exerciseDuration = exerciseDuration;
    }

    public List<Object> getExerciseList() {
        return exerciseList;
    }

    public void setExerciseList(List<Object> exerciseList) {
        this.exerciseList = exerciseList;
    }

    public int getExerciseRest() {
        return exerciseRest;
    }

    public void setExerciseRest(int exerciseRest) {
        this.exerciseRest = exerciseRest;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public int getRecommendedRounds() {
        return recommendedRounds;
    }

    public void setRecommendedRounds(int recommendedRounds) {
        this.recommendedRounds = recommendedRounds;
    }

    public String getWorkoutDescription() {
        return workoutDescription;
    }

    public void setWorkoutDescription(String workoutDescription) {
        this.workoutDescription = workoutDescription;
    }

    public String getWorkoutImage() {
        return workoutImage;
    }

    public void setWorkoutImage(String workoutImage) {
        this.workoutImage = workoutImage;
    }

    public String getWorkoutTitle() {
        return workoutTitle;
    }

    public void setWorkoutTitle(String workoutTitle) {
        this.workoutTitle = workoutTitle;
    }

    public String getWorkoutVideo() {
        return workoutVideo;
    }

    public void setWorkoutVideo(String workoutVideo) {
        this.workoutVideo = workoutVideo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ParseObject getParseWorkout() {
        return parseWorkout;
    }

    public void setParseWorkout(ParseObject parseWorkout) {
        this.parseWorkout = parseWorkout;
    }
}
