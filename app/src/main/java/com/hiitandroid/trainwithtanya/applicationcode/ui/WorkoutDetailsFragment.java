/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hiitandroid.trainwithtanya.applicationcode.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hiitandroid.trainwithtanya.applicationcode.R;
import com.hiitandroid.trainwithtanya.applicationcode.util.ImageFetcher;
import com.hiitandroid.trainwithtanya.applicationcode.util.ImageWorker;
import com.hiitandroid.trainwithtanya.applicationcode.util.Utils;

import org.w3c.dom.Text;

/**
 * This fragment will populate the children of the ViewPager from {@link WorkoutDetailsActivity}.
 */
public class WorkoutDetailsFragment extends Fragment {
    private static final String IMAGE_DATA_EXTRA = "extra_image_data";
    private static final String WORKOUT_DESCRIPTION = "description";
    private static final String WORKOUT_TITLE = "title";
    private static final String WORKOUT_ROUNDS = "rounds";
    private String mImageUrl;
    private String mDescription;
    private ImageView mImageView;
    private Button mStartButton;
    private ImageFetcher mImageFetcher;
    private TextView mDescriptionView;
    private String mWorkoutTitle;
    private int mRounds;
    private TextView mTitleView;
    private TextView mSpecsView;
    private Button mBackButton;

    private OnStartWorkoutListener mListener;

    /**
     * Factory method to generate a new instance of the fragment given an image number.
     *
     * @param imageUrl The image url to load
     * @return A new instance of WorkoutDetailsFragment with imageNum extras
     */
    public static WorkoutDetailsFragment newInstance(String imageUrl, String workoutDescription, String workoutTitle, int recommendedRounds) {
        final WorkoutDetailsFragment f = new WorkoutDetailsFragment();

        final Bundle args = new Bundle();
        args.putString(IMAGE_DATA_EXTRA, imageUrl);
        args.putString(WORKOUT_DESCRIPTION, workoutDescription);
        args.putString(WORKOUT_TITLE, workoutTitle);
        args.putInt(WORKOUT_ROUNDS, recommendedRounds);

        f.setArguments(args);

        return f;
    }

    /**
     * Empty constructor as per the Fragment documentation
     */
    public WorkoutDetailsFragment() {}

    /**
     * Populate image using a url from extras, use the convenience factory method
     * {@link WorkoutDetailsFragment#newInstance(String, String, String, int)} to create this fragment.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mImageUrl = getArguments() != null ? getArguments().getString(IMAGE_DATA_EXTRA) : null;
        mDescription = getArguments() != null ? getArguments().getString(WORKOUT_DESCRIPTION) : null;
        mWorkoutTitle = getArguments() != null ? getArguments().getString(WORKOUT_TITLE) : null;
        mRounds = getArguments() != null ? getArguments().getInt(WORKOUT_ROUNDS) : null;


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate and locate the main ImageView
        final View v = inflater.inflate(R.layout.workout_details_fragment, container, false);
        mImageView = (ImageView) v.findViewById(R.id.imageView);
        mStartButton = (Button) v.findViewById(R.id.startWorkoutButton);
        mDescriptionView = (TextView) v.findViewById(R.id.workout_description);
        mTitleView = (TextView) v.findViewById(R.id.details_workout_title);
        mSpecsView = (TextView) v.findViewById(R.id.details_workout_specs);
        mBackButton = (Button) v.findViewById(R.id.details_back_button);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnStartWorkoutListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnStartWorkoutListener");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Use the parent activity to load the image asynchronously into the ImageView (so a single
        // cache can be used over all pages in the ViewPager
        if (WorkoutDetailsActivity.class.isInstance(getActivity())) {
            mImageFetcher = ((WorkoutDetailsActivity) getActivity()).getImageFetcher();
            mImageFetcher.loadImage(mImageUrl, mImageView);
            mStartButton.setVisibility(View.VISIBLE);
            mDescriptionView.setText(mDescription);
            mTitleView.setText(mWorkoutTitle);
            mSpecsView.setText(getActivity().getResources().getQuantityString(R.plurals.specs, mRounds, mRounds));
            mStartButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onStartWorkout();
                }
            });
            mBackButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });
        }

        // Pass clicks on the ImageView to the parent activity to handle
        if (OnClickListener.class.isInstance(getActivity()) && Utils.hasHoneycomb()) {
            mImageView.setOnClickListener((OnClickListener) getActivity());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mImageView != null) {
            // Cancel any pending image work
            ImageWorker.cancelWork(mImageView);
            mImageView.setImageDrawable(null);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnStartWorkoutListener {
        // TODO: Update argument type and name
        public void onStartWorkout();
    }
}
