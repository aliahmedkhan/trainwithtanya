package com.hiitandroid.trainwithtanya.applicationcode.ui;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.hiitandroid.trainwithtanya.applicationcode.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ExerciseModule.OnModuleFinishedListener} interface
 * to handle interaction events.
 * Use the {@link ExerciseModule#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExerciseModule extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TITLE = "mExerciseTitle";
    private static final String ARG_URL = "mVideoUrl";
    private static final String ARG_ROUND = "mRound";
    private static final String ARG_TOTAL_ROUNDS = "mTotalRounds";
    private static final String ARG_EXERCISE_DURATION = "mExerciseDuration";
    private static final String ARG_EXERCISE_NUMBER = "mExerciseNumber ";

    // TODO: Rename and change types of parameters
    private String mExerciseTitle;
    private String  mVideoUrl;
    private int     mRound;
    private int     mTotalRounds;
    private int     mExerciseDuration;
    private VideoView mVideo;
    private CountDownTimer mCountDownTimer;
    private int mExerciseNumber;


    private OnModuleFinishedListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param workoutTitle Parameter 1.
     * @param videoUrl Parameter 2.
     * @return A new instance of fragment ExerciseModule.
     */
    // TODO: Rename and change types and number of parameters
    public static ExerciseModule newInstance(String workoutTitle, String videoUrl, int round, int totalRounds, int exerciseDuration, int exerciseNumber) {
        ExerciseModule fragment = new ExerciseModule();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, workoutTitle);
        args.putString(ARG_URL, videoUrl);
        args.putInt(ARG_ROUND, round);
        args.putInt(ARG_TOTAL_ROUNDS, totalRounds);
        args.putInt(ARG_EXERCISE_DURATION, exerciseDuration);
        args.putInt(ARG_EXERCISE_NUMBER, exerciseNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ExerciseModule() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mExerciseTitle = getArguments().getString(ARG_TITLE);
            mVideoUrl = getArguments().getString(ARG_URL);
            mRound = getArguments().getInt(ARG_ROUND);
            mTotalRounds = getArguments().getInt(ARG_TOTAL_ROUNDS);
            mExerciseDuration = getArguments().getInt(ARG_EXERCISE_DURATION);
            mExerciseNumber = getArguments().getInt(ARG_EXERCISE_NUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exercise_module, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView end = (TextView) getView().findViewById(R.id.module_end);
        end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onWorkoutExited();
            }
        });
        TextView moduleTitle = (TextView) getView().findViewById(R.id.module_workoutTitle);
        moduleTitle.setText(mExerciseTitle);
        TextView roundNumber = (TextView) getView().findViewById(R.id.module_round);
        roundNumber.setText(Integer.toString(mRound));
        final TextView timer = (TextView) getView().findViewById(R.id.module_timer);
        final ProgressBar pb = (ProgressBar) getView().findViewById(R.id.workoutProgressBar);
        pb.post(new Runnable() {
            @Override
            public void run() {
                pb.setMax(102);
                pb.setProgress((mExerciseNumber) * 17);
                pb.setSecondaryProgress((mExerciseNumber + 1) * 17);
            }
        });

        startVideo();
        mCountDownTimer = new CountDownTimer((mExerciseDuration * 1000) + 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timer.setText(Integer.toString((int) millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                if (mListener != null) {
                    mVideo.suspend();
                    mListener.onModuleFinished();
                }
            }
        }.start();
    }

    private void startVideo() {
        Uri uri = Uri.parse(mVideoUrl);
        mVideo = (VideoView) getView().findViewById(R.id.module_video);
        mVideo.setVideoURI(uri);
        mVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        mVideo.start();

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onModuleFinished();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnModuleFinishedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnModuleFinishedListener");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mListener.onWorkoutExited();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mVideo.destroyDrawingCache();
        Log.v("", "detach module occurs");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnModuleFinishedListener {
        public void onModuleFinished();
        public void onWorkoutExited();
    }

}
